package ru.jkh.feedback;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.jkh.pages.FeedbackObject;
import java.util.concurrent.TimeUnit;

public class feedbackinsertTest {
    // ввод верных данных в форму восстановл пароля
    private static WebDriver driver;
    public static FeedbackObject feedbackobject;

    @BeforeClass
    public static void setup() {

        //     System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }
    //ввод данных в форму восстановл пароля
    @Test
    public void feedbacktestinsert() {
        feedbackobject = new FeedbackObject(driver);
        feedbackobject.autoficationclick();
        feedbackobject.setLoginField();
        feedbackobject.setPasswordField();
        feedbackobject.setLoginButton();
        driver.navigate().refresh();

        feedbackobject.setInsertname();
        feedbackobject.setInseremail();
        feedbackobject.setInsertmailtext();
        feedbackobject.setButtonclick();
        driver.quit();
    }
}
