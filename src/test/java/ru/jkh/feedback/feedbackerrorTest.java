package ru.jkh.feedback;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.jkh.pages.FeedbackObject;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

public class feedbackerrorTest {
    // ввод неверных данных в форму восстановл пароля
    private static WebDriver driver;
    public static FeedbackObject feedbackobject;

    @BeforeClass
    public static void setup() {

        //System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }
    @Test
    // ввод неверных данных в форму восстановл пароля
    public void feedbacktesterror() {
        feedbackobject = new FeedbackObject(driver);
        String[] t = {"Обратная связь", "Укажите ваше имя.", "Укажите E-mail, на который хотите получить ответ.",
                "Вы не написали сообщение.", "Указанный E-mail некорректен." , "Не указан код защиты от автоматических сообщений."};
        feedbackobject.setButtonclick();
        assertEquals(t[0].length(), feedbackobject.feedbackFormtitle.getText().length());
        assertEquals(t[1].length(), feedbackobject.texterror1.getText().length());
        assertEquals(t[2].length(), feedbackobject.texterror2.getText().length());
        assertEquals(t[3].length(), feedbackobject.texterror3.getText().length());
        assertEquals(t[4].length(), feedbackobject.texterror4.getText().length());
        assertEquals(t[5].length(), feedbackobject.texterror5.getText().length());
        driver.quit();
    }
}
