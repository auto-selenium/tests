package ru.jkh.meters;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import ru.jkh.pages.metersObject;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

public class insertmetersTest {      // ввод данных для 1 счетчика
    private static WebDriver driver;
    public static metersObject metersobject;

    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }
    // проверка на ввод показаний счетчика
    @Test
    public void insertmeters() {
        metersobject = new metersObject(driver);
        metersobject.setAutofication();
        metersobject.setLoginField("minery");
        metersobject.setPasswordField("123123");
        metersobject.setLoginButton();

        driver.navigate().refresh();
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(metersobject.lc).build().perform();
        metersobject.setMetersmenu();
        metersobject.setInsertmeters();
        metersobject.setInsertmeters2();
        metersobject.setMetersclickbutton();
        driver.quit();
    }
}
