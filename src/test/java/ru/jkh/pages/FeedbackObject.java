package ru.jkh.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FeedbackObject {     //класс веб-элементов страницы восстановления пароля
    public FeedbackObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public WebDriver driver;

    //форма восстановления пароля
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[1]")
    public WebElement feedbackFormtitle;

    @FindBy(xpath = "//*[@id=\"contact-user-name\"]")
    public WebElement insertname;

    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/input[2]")
    public WebElement inseremail;

    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/textarea")
    public WebElement insertmailtext;

    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/table/tbody/tr/td[2]/input")
    public WebElement insertcaptcha;

    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/button")
    public WebElement buttonclick;

    //ошибки

    //Укажите ваше имя.
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/div[1]")
    public WebElement texterror1;

    //Укажите E-mail, на который хотите получить ответ.
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/div[2]")
    public WebElement texterror2;

    //Вы не написали сообщение.
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/div[3]")
    public WebElement texterror3;

    //Указанный E-mail некорректен.
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/div[4]")
    public WebElement texterror4;

    //Не указан код защиты от автоматических сообщений.
    @FindBy(xpath = "//*[@id=\"feedbackForm\"]/div/div[2]/div[5]")
    public WebElement texterror5;


    //авторизация
    @FindBy(linkText = "Авторизация")
    private WebElement autofication;

    @FindBy(name = "USER_LOGIN")
    private WebElement loginField;

    @FindBy(name = "USER_PASSWORD")
    private WebElement passwordField;

    @FindBy(className = "window__button")
    private WebElement loginButton;

    //тестовые методы восстановл пароля
    public void setInsertname(){insertname.sendKeys("minery");}
    public void setInseremail(){inseremail.sendKeys("minery1@yandex.ru");}
    public void setInsertmailtext(){insertmailtext.sendKeys("тест проверка");}
    public void setInsertcaptcha(){insertcaptcha.sendKeys("QOESA3");}
    public void setButtonclick(){buttonclick.click();}

    public void autoficationclick(){autofication.click();}
    public void setLoginField(){loginField.sendKeys("admin");}
    public void setPasswordField(){passwordField.sendKeys("rarus22");}
    public void setLoginButton(){loginButton.click();}

}
