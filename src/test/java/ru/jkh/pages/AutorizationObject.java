package ru.jkh.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutorizationObject {  //класс веб-элементов формы "авторизация" и авторизации через л/к
    public AutorizationObject(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public WebDriver driver;

    @FindBy(linkText = "Авторизация")
    private WebElement autofication;

    @FindBy(className = "window__title")
    public WebElement titleautofication;

    //текст - всстановление пароля
    @FindBy(xpath = "//*[@id=\"window-password-recovery\"]/form/div[1]")
    public WebElement passwordrecovery;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[3]/div[1]/div")
    public WebElement messagelogin;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[3]/div[2]/div[1]")
    public WebElement messagepassword;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[3]/div[2]/div[2]/a")
    public WebElement clickmessage;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[4]/label/span[1]")
    public WebElement clickicon;

    @FindBy(className = "networks__title")
    public WebElement networktitle;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/div[5]/a")
    public WebElement textfooter;

    @FindBy(xpath = "//*[@id=\"bx_auth_href_formMailRuOpenID\"]/img")
    public WebElement mailicon;

    @FindBy(xpath = "//*[@id=\"bx_auth_href_formLivejournal\"]/img")
    public WebElement livejournalicon;

    @FindBy(className = "window__input")
    private WebElement loginField;

    @FindBy(name = "USER_PASSWORD")
    private WebElement passwordField;

    @FindBy(className = "window__button")
    private WebElement loginButton;

    @FindBy(className = "window__description_bold")
    public WebElement description;

    @FindBy(xpath = "//*[@id=\"window-password-recovery\"]/form/div[3]/text()")  //*[@id="window-password-recovery"]/form/div[3]/text()
    public WebElement description1;

    @FindBy(xpath = "//*[@id=\"window__inputs-recovery\"]/div[1]/div")
    public WebElement textlogin;

    @FindBy(xpath = "//*[@id=\"window__inputs-recovery\"]/div[2]/div")
    public WebElement textmail;

    @FindBy(xpath = "//*[@id=\"window__sendbutton-recovery\"]")  //*[@id="window__sendbutton-recovery"]/value
    public WebElement sendbutton;

    //или
    @FindBy(xpath = "//*[@id=\"window__text_or-recovery\"]")
    public WebElement textor;

    //войти
    @FindBy(xpath = "//*[@id=\"window__enter-recovery\"]")
    public WebElement textcome;

    @FindBy(xpath = "//*[@id=\"window__inputs-recovery\"]/div[1]/input")
    public WebElement windowsintput;

    @FindBy(xpath = "//*[@id=\"window__inputs-recovery\"]/div[2]/input")
    public WebElement windowsemailrecovery;

    @FindBy(xpath = "//*[@id=\"window-password-recovery\"]/form/div[6]")
    public WebElement textpasswordrecovery;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[6]")
    public WebElement texterror;

    @FindBy(xpath = "/html/body/header/div[1]/div/span/span[1]/a")
    public WebElement registration;

    @FindBy(className = "//*[@id=\"window-register-container\"]/form/div[1]")
    public WebElement registrationtitle;

    //учетные данные
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[1]")
    public WebElement fieldgroup;

    //login
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[2]/div[1]")
    public WebElement registerlogin;

    //password
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[3]/div[1]")
    public WebElement registerpassword;

    //repeat password
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[4]/div")
    public WebElement registerpasswordrepeat;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[1]")
    public WebElement fieldgroup2;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[2]/div")
    public WebElement name1;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[3]/div")
    public WebElement otchestvo;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[4]/div")
    public WebElement family;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[5]/div")
    public WebElement e_mailregistr;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[6]/div[1]")
    public WebElement phoneregistr;
    //последний заголовок
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[3]/div[1]")
    public WebElement fieldgroup3;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[2]/input")
    public WebElement insertlogin;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[3]/input")
    public WebElement insertpassword;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[4]/input")
    public WebElement insertpasswordrepeat;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[2]/input")
    public WebElement insertname;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[3]/input")
    public WebElement inserochetvo;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[4]/input")
    public WebElement insertfamily;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[5]/input")
    public WebElement insertemail;

    @FindBy(xpath = "//*[@id=\"customer_phone\"]")
    public WebElement insertphone;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[3]/div[2]/div[2]/input")
    public WebElement insertcaptcha;

    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[4]/input[2]")
    public WebElement clickregistration;

    //авторизация через меню - л/к


    @FindBy(xpath = "//*[@id=\"menu_top\"]/div/ul/li[4]/a")
    public WebElement menulc;

    // Доступ запрещен. Пожалуйста, авторизуйтесь.
    @FindBy(xpath = "/html/body/div[1]/div/div/p/font")
    public WebElement acsbacklc;

    //Войти в личный кабинет
    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[1]")
    public WebElement inputlc;

    //Запомнить меня на этом компьютере
    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[3]/label/span[1]")
    public WebElement savemylc;

    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[2]/input[1]")
    public WebElement userloginlc;

    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[2]/input[2]")
    public WebElement userpasswordlc;

    //заопнить меня - клик по чекбоксу
    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[3]/label")
    public WebElement labelclicklc;

    @FindBy(xpath = "/html/body/div[2]/form/div/div/div[5]/input")
    public WebElement inputclicklc;

    @FindBy(xpath = "//*[@id=\"window-auth\"]/form/div[6]")
    public  WebElement errorlc;

    //ошибки при регистрации

    //вывод заголовка - При отправке формы произошли ошибки, пожалуйста, проверьте правильность заполнения полей и повторите попытку.
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[3]/div[1]")
    public WebElement errortitleregistr;

    //Неверно введено слово с картинки
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[3]/div[2]")
    public WebElement errortitle2registr;

    //Поле "Логин" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[2]/div[3]")
    public WebElement errorloginregistr;

    //Поле "Пароль" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[3]/div[3]")
    public WebElement errorpasswordregistr;

    //Поле "Повторите пароль" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[1]/div[4]/div[2]")
    public WebElement errorpaswordrepeatregistr;
    //Поле "Имя" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[2]/div[2]")
    public WebElement errornameregistr;

    //Поле "Фамилия" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[4]/div[2]")
    public  WebElement errorfamilyregistr;

    //Поле "E-mail" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[5]/div[2]")
    public WebElement erroremailregistr;

    //Поле "Телефон" обязательно для заполнения
    @FindBy(xpath = "//*[@id=\"window-register-container\"]/form/div[4]/div[2]/div[6]/div[3]")
    public WebElement errorphoneregistr;

    //методы для авторизации, через форму "Авторизация"
    public void autoficationclick(){autofication.click();}
    public void setclickmessage(){clickmessage.click();}
    public void setClickicon(){clickicon.click();}
    public void clickmailicon(){mailicon.click();}
    public void clicklivejournalicon(){livejournalicon.click();}
    public void inputLogin(String login) { loginField.sendKeys(login); }
    public void inputPassword(String password) {
        passwordField.sendKeys(password);
    }
    public void clickLoginButton() { loginButton.click(); }
    public void setWindowsintput(){windowsintput.sendKeys("abc");}
    public void setWindowsemailrecovery(){windowsemailrecovery.sendKeys("abc");}
    public void setSendbutton(){sendbutton.click();}

    //методы для регистрации
    public void setRegistration(){registration.click();}
    public void setInsertlogin(){insertlogin.sendKeys("testik");}
    public void setInsertpassword(){insertpassword.sendKeys("123456");}
    public void setInsertpasswordrepeat(){insertpasswordrepeat.sendKeys("123456");}
    public void setInsertname(){insertname.sendKeys("oleg");}
    public void setInserochetvo(){inserochetvo.sendKeys("olegan");}
    public void setInsertfamily(){insertfamily.sendKeys("oleganov");}
    public void setInsertemail(){insertemail.sendKeys("test12@test.ru");}
    public void setInsertphone(){insertphone.sendKeys("79218810545");}
    public void setInsertcaptcha(){insertcaptcha.sendKeys("243145");}
    public void setClickregistration(){clickregistration.click();}

    //Методы для авторизации через личный кабнет
    public void setMenulc(){menulc.click();}
    public void setUserloginlc(){userloginlc.sendKeys("admin");}
    public void setUserpasswordlc(){userpasswordlc.sendKeys("rarus22");}
    public void setUserloginlcerror(){userloginlc.sendKeys("123");}
    public void setUserpasswordlcerror(){userpasswordlc.sendKeys("123");}
    public void setLabelclicklc(){labelclicklc.click();}
    public void setInputclicklc(){inputclicklc.click();}

}
