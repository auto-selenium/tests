package ru.jkh.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class metersObject {  //класс веб-элементов страницы показаний счетчиков
    public metersObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public WebDriver driver;

    @FindBy(linkText = "Авторизация")
    private WebElement autofication;

    @FindBy(name = "USER_LOGIN")
    private WebElement loginField;

    @FindBy(name = "USER_PASSWORD")
    private WebElement passwordField;

    @FindBy(className = "window__button")
    private WebElement loginButton;

    @FindBy(linkText = "Личный кабинет")
    public static WebElement lc;

    @FindBy(xpath = "//*[@id=\"menu_top\"]/div/ul/li[4]/div/ul/li[4]/a")
    public WebElement metersmenu;

    @FindBy(xpath = "/html/body/div[1]/div/div/div[2]/div[2]/div[1]")
    public WebElement title;

    @FindBy(xpath = "/html/body/div[1]/div/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div[3]/div/div/div[2]/div/input")
    public WebElement insertmeters;

    @FindBy(xpath = "/html/body/div[1]/div/div/div[2]/div[2]/div[2]/form/div[1]/div[3]/div[3]/div/div/div[2]/div/input")
    public WebElement insertmeters2;

    @FindBy(xpath = "/html/body/div/div/div/div[2]/div[2]/div[2]/form/div[2]/input")
    public WebElement metersclickbutton;

    //методы ввода счетчиков
    public void setMetersmenu(){metersmenu.click();}
    public void setAutofication(){autofication.click();}
    public void setPasswordField(String password){passwordField.sendKeys(password);}
    public void setLoginField(String login){loginField.sendKeys(login);}
    public void setLoginButton(){loginButton.click();}
    public void setInsertmeters(){insertmeters.sendKeys("12");}
    public void setInsertmeters2(){insertmeters2.sendKeys("15");}
    public void setMetersclickbutton(){metersclickbutton.click();}
}
