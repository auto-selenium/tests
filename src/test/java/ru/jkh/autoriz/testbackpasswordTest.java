package ru.jkh.autoriz;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;
import ru.jkh.pages.AutorizationObject;
import java.util.concurrent.TimeUnit;

public class testbackpasswordTest {   //проверка на вывод сообщений об ошибке у формы восстановл пароля
    private static WebDriver driver;
    public static AutorizationObject autorizobject;

    @BeforeClass
    public static void setup() {

        //     System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }

    @Test
    public void back_password()  {
        autorizobject = new AutorizationObject(driver);
        autorizobject.autoficationclick();
        autorizobject.setclickmessage();

        String[] t = {"Восстановление пароля", "Если вы забыли пароль, введите логин или E-Mail.", "Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.", "Логин *", "E-Mail *",
                "отправить", "или", "Войти", "Логин или EMail не найдены."};
        // проверки формы восстановл пароля, если введены неверные данные, если утверждение неверное, то вывод ошибки
        assertEquals(t[0].length(), autorizobject.passwordrecovery.getText().length());
        assertEquals(t[1].length(), autorizobject.description.getText().length());

        assertEquals(t[3].length(), autorizobject.textlogin.getText().length());
        assertEquals(t[4].length(), autorizobject.textmail.getText().length());

        assertEquals(t[6].length(), autorizobject.textor.getText().length());
        assertEquals(t[7].length(), autorizobject.textcome.getText().length());

        autorizobject.setWindowsemailrecovery();
        autorizobject.setSendbutton();
        autorizobject.setWindowsintput();
        driver.close();
    }
}
