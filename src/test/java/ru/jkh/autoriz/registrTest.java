package ru.jkh.autoriz;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;
import ru.jkh.pages.AutorizationObject;
import java.util.concurrent.TimeUnit;

public class registrTest {  // ввод данных в форму регистрации, а также проверка названий форм
    private static WebDriver driver;
    public static AutorizationObject autorizobject;

    @BeforeClass
    public static void setup() {

        //     System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }

    @Test
    public void regist() {
        // методы - ввод данных в формы регристрации
        autorizobject = new AutorizationObject(driver);
        autorizobject.setRegistration();
        autorizobject.setInsertlogin();
        autorizobject.setInsertpassword();
        autorizobject.setInsertpasswordrepeat();
        autorizobject.setInsertname();
        autorizobject.setInserochetvo();
        autorizobject.setInsertfamily();
        autorizobject.setInsertemail();
        autorizobject.setInsertphone();
        autorizobject.setInsertcaptcha();

        String[] t = {"Регистрация", "Учетные данные", "Логин:*", "Пароль:*", "Повторите пароль:*", "Персональные данные",
                "Имя:*", "Отчество:", "Фамилия:*", "E-mail:*", "Телефон:*", "Защита от автоматической регистрации"};

      //  assertEquals(t[0].length(), autorizobject.registrationtitle.getText().length());
        //    localtests.p = autorizobject.registrationtitle.getText();
        // проверки на совпадение текста
        assertEquals(t[1].length(), autorizobject.fieldgroup.getText().length());
        assertEquals(t[2].length(), autorizobject.registerlogin.getText().length());
        assertEquals(t[3].length(), autorizobject.registerpassword.getText().length());
        assertEquals(t[4].length(), autorizobject.registerpasswordrepeat.getText().length());
        assertEquals(t[5].length(), autorizobject.fieldgroup2.getText().length());
        assertEquals(t[6].length(), autorizobject.name1.getText().length());
        assertEquals(t[7].length(), autorizobject.otchestvo.getText().length());
        assertEquals(t[8].length(), autorizobject.family.getText().length());
        assertEquals(t[9].length(), autorizobject.e_mailregistr.getText().length());
        assertEquals(t[10].length(), autorizobject.phoneregistr.getText().length());
        assertEquals(t[11].length(), autorizobject.fieldgroup3.getText().length());
        driver.close();

    }
}
