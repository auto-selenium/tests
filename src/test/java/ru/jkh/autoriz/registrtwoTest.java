package ru.jkh.autoriz;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;
import ru.jkh.pages.AutorizationObject;
import java.util.concurrent.TimeUnit;

public class registrtwoTest {    //ввод пустых данных формы регистрации, проверка на неправильность вывода сообщений об ошибке
    private static WebDriver driver;
    public static AutorizationObject autorizobject;

    @BeforeClass
    public static void setup() {

        //     System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        driver.get("http://unit.demo-jkh.ru");
    }

    @Test
    public void registtwo() {
        autorizobject = new AutorizationObject(driver);
        String[] t = {"При отправке формы произошли ошибки, пожалуйста, проверьте правильность заполнения полей и повторите попытку.", "Неверно введено слово с картинки", "Поле \"Логин\" обязательно для заполнения", "Поле \"Пароль\" обязательно для заполнения", "Поле \"Повторите пароль\" обязательно для заполнения",
                "Поле \"Имя\" обязательно для заполнения", "Поле \"Фамилия\" обязательно для заполнения", "Поле \"E-mail\" обязательно для заполнения", "Поле \"Телефон\" обязательно для заполнения"};
        autorizobject.setRegistration();
        autorizobject.setClickregistration();
        //проверки формы авторизации, если данные введены не верно, если утверждение неверное, то ошибка
        assertEquals(t[0].length(), autorizobject.errortitleregistr.getText().length());
        assertEquals(t[1].length(), autorizobject.errortitle2registr.getText().length());
        assertEquals(t[2].length(), autorizobject.errorloginregistr.getText().length());
        assertEquals(t[3].length(), autorizobject.errorpasswordregistr.getText().length());
        assertEquals(t[4].length(), autorizobject.errorpaswordrepeatregistr.getText().length());
        assertEquals(t[5].length(), autorizobject.errornameregistr.getText().length());
        assertEquals(t[6].length(), autorizobject.errorfamilyregistr.getText().length());
        assertEquals(t[7].length(), autorizobject.erroremailregistr.getText().length());
        assertEquals(t[8].length(), autorizobject.errorphoneregistr.getText().length());
        driver.quit();
    }
}